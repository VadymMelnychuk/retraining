﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Import log4net classes.
using log4net;
using log4net.Config;
using FirstClassLib.Reflection;
using FirstClassLib.EntityFramework;
using AutoMapper;
using SimpleInjector;

namespace FirstConsoleApp
{
    class AlfaClass
    {
        public string Name { get; set; }
    }

    class BetaClass
    {
        private readonly AlfaClass alfa;

        public BetaClass(AlfaClass a)
        {
            this.alfa = a;
            Res = "Bet result with Alfa";
        }

        public string Res { get; set; }
    }

    class Program
    {
        // Define a static logger variable so that it references the
        // Logger instance named "MyApp".
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void TestReflection()
        {
            ReflectionTests reflTests = new ReflectionTests();
            reflTests.Test1();
            reflTests.Test2();
        }

        static void TestEntityFramework()
        {
            EntityFrameworkTests efTests = new EntityFrameworkTests();
            //efTests.Test_AddProduct();
            //efTests.Test_DispalyProducts();
            efTests.Test_InsertPerfomance();
        }

        static void Test_NPlus1Problem()
        {
            TestNPlus1Problem test = new TestNPlus1Problem();
            test.CalculateTests();
            Console.ReadKey();
        }

        static void TestInjections()
        {
            Container cnt = new Container();
            cnt.Register<AlfaClass, AlfaClass>();
            cnt.Register<BetaClass, BetaClass>();


            Console.WriteLine(cnt.GetInstance<BetaClass>().Res);
            Console.ReadKey();
        }

        static void TestJoin()
        {
            string[] cats = new string[] { "aa", "bb", "cc" };
            var prods = new[] { new { cat = "aa", name = "First" }, new { cat = "aa", name = "First2" }, new { cat = "bb", name = "Second" } };


            var q =
                from c in cats
                join p in prods on c equals p.cat into ps
                from p in ps.DefaultIfEmpty()
                where p is null
                select new { Category = c, ProductName = p == null ? "(No products)" : p.name };

            foreach (var v in q)
            {
                Console.WriteLine(v.Category + ": " + v.ProductName);
            }

            Console.ReadKey();

        }

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            log.Info("Entering application.");
            //Test_NPlus1Problem();

            TestJoin();

            //TestInjections();

            log.Info("Exiting application.");
        }
    }
}
