﻿using System.Collections.Generic;

namespace DotNetTraining.ApplicationFacade
{
    public interface IHotelFacade
    {
        List<HotelFacadeItem> All();
        void Delete(int id);
        HotelFacadeItem HotelById(int id);
        void Add(HotelFacadeItem value);
        void Update(int id, HotelFacadeItem value);
    }
}