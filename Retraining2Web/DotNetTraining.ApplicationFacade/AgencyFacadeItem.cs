﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DotNetTraining.ApplicationFacade
{
    public class AgencyFacadeItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [Display(Name = "Agency Logo Image")]
        public string LogoImageName { get; set; }
    }
}
