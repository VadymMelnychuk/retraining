﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetTraining.BusinessLogic;

namespace DotNetTraining.ApplicationFacade
{
    public interface ILocationFacade : IBaseFacadeInterface<LocationFacadeItem, LocationLogicItem>
    {
        IList<LocationFacadeItem> All();
        IList<T> All<T>();
        void Save(LocationFacadeItem item);
        LocationFacadeItem ItemById(string id);
        void Delete(string id);
        bool ValidateLocationName(string locationName);
    }
}
