﻿using DotNetTraining.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.ApplicationFacade
{
    public class HotelFacade : BaseFacadeImpl<HotelFacadeItem, HotelLogicItem>, IHotelFacade
    {
        private readonly IHotelLogic hotelLogic;

        public HotelFacade(IHotelLogic hotelLogic)
        {
            this.hotelLogic = hotelLogic;
        }

        public List<HotelFacadeItem> All()
        {
            return hotelLogic.All().ConvertAll<HotelFacadeItem>(h => Logic2Facade(h));
        }
        public HotelFacadeItem HotelById(int id)
        {
            return Logic2Facade(hotelLogic.HotelById(id));
        }
        public void Delete(int id)
        {
            hotelLogic.Delete(id);
        }

        public void Add(HotelFacadeItem value)
        {
            hotelLogic.Add(Facade2Logic(value));
        }

        public void Update(int id, HotelFacadeItem value)
        {
            hotelLogic.Update(id, Facade2Logic(value));
        }
    }
}
