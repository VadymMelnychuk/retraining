﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DotNetTraining.BusinessLogic;

namespace DotNetTraining.ApplicationFacade
{
    public class LocationFacade : BaseFacadeImpl<LocationFacadeItem, LocationLogicItem>, ILocationFacade
    {
        private readonly ILocationLogic locationLogic;

        public LocationFacade(ILocationLogic locationLogic)
        {
            this.locationLogic = locationLogic;
        }
        public IList<LocationFacadeItem> All()
        {
            List<LocationFacadeItem> list = new List<LocationFacadeItem>();
            foreach (var item in locationLogic.All())
                list.Add(Logic2Facade(item));
            return list;
        }
        public void Save(LocationFacadeItem item)
        {
            locationLogic.Save(Facade2Logic(item));
        }

        public LocationFacadeItem ItemById(string id)
        {
            return All().FirstOrDefault(x => x.Id == id);
        }

        public void Delete(string id)
        {
            locationLogic.Delete(id);
        }

        public bool ValidateLocationName(string locationName)
        {
            return locationLogic.ValidateLocationName(locationName);
        }

        public IList<T> All<T>()
        {
            return locationLogic.All<T>();
        }
    }
}
