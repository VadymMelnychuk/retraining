﻿using DotNetTraining.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.ApplicationFacade
{
    public interface IAgencyFacade : IBaseFacadeInterface<AgencyFacadeItem, AgencyLogicItem>
    {
        AgencyFacadeItem AgencyById(int agencyId);
        void Save(AgencyFacadeItem agency);
    }
}
