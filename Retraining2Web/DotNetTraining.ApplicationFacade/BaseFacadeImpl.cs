﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.ApplicationFacade
{
    public class BaseFacadeImpl<TFacadeItem, TLogicItem> : IBaseFacadeInterface<TFacadeItem, TLogicItem>
    {
        public TFacadeItem Logic2Facade(TLogicItem logicItem)
        {
            return Mapper.Map<TLogicItem, TFacadeItem>(logicItem);
        }

        public TLogicItem Facade2Logic(TFacadeItem facadeItem)
        {
            return Mapper.Map<TFacadeItem, TLogicItem>(facadeItem);
        }
    }
}
