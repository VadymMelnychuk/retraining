﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DotNetTraining.ApplicationFacade
{
    public class LocationFacadeItem
    {
        public string Id { get; set; }
        [Display(Name = "Location Name")]
        [Required]
        [Remote("ValidateLocationName", "Location", HttpMethod = "POST", ErrorMessage = "Location Name should be unique")]
        public string LocationName { get; set; }
        [Required]
        [Display(Name = "Country")]
        public string CountryId { get; set; }
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
    }
}
