﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.ApplicationFacade
{
    public class HotelFacadeItem
    {
        public int Id { get; set; }
        [Display(Name = "Hotel Name")]
        public string Name { get; set; }
        public int Stars { get; set; }
        [Display(Name = "Location Name")]
        public string LocationId { get; set; }
        public string LocationName { get; set; }
    }
}
