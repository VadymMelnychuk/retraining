﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetTraining.BusinessLogic;

namespace DotNetTraining.ApplicationFacade
{
    public class AgencyFacade : BaseFacadeImpl<AgencyFacadeItem, AgencyLogicItem>, IAgencyFacade
    {
        private readonly IAgencyLogic agencyLogic;

        public AgencyFacade(IAgencyLogic agencyLogic)
        {
            this.agencyLogic = agencyLogic;
        }

        public AgencyFacadeItem AgencyById(int agencyId)
        {
            return Logic2Facade(agencyLogic.AgencyById(agencyId));
        }
        public void Save(AgencyFacadeItem agency)
        {
            agencyLogic.Save(Facade2Logic(agency));
        }

    }
}
