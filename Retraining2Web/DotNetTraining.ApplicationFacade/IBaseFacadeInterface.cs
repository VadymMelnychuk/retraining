﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.ApplicationFacade
{
    public interface IBaseFacadeInterface<TFacadeItem, TLogicItem>
    {
        TFacadeItem Logic2Facade(TLogicItem logicItem);
        TLogicItem Facade2Logic(TFacadeItem facadeItem);
    }
}
