﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.BusinessLogic
{
    public class LocationLogicItem
    {
        public string Id { get; set; }
        public string LocationName { get; set; }
        public string CountryId { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}
