﻿using DotNetTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetTraining.DAL.Reprositories;

namespace DotNetTraining.BusinessLogic
{
    public class AgencyLogic : BaseLogicImpl<AgencyLogicItem, Agency>, IAgencyLogic
    {
        private readonly AgencyRepo agencyRepo;

        public AgencyLogic(AgencyRepo agencyRepo)
        {
            this.agencyRepo = agencyRepo;
        }

        public AgencyLogicItem AgencyById(int agencyId)
        {
            return Entity2LogicItem(agencyRepo.AgencyById(agencyId));
        }
        public void Save(AgencyLogicItem agency)
        {
            agencyRepo.Save(LogicItem2Entity(agency));
        }

    }
}
