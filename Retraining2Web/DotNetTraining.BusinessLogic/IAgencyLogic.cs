﻿using DotNetTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.BusinessLogic
{
    public interface IAgencyLogic: IBaseLogicInterface<AgencyLogicItem, Agency>
    {
        AgencyLogicItem AgencyById(int agencyId);
        void Save(AgencyLogicItem agency);
    }
}
