﻿using DotNetTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.BusinessLogic
{
    public interface ILocationLogic : IBaseLogicInterface<LocationLogicItem, RestLocation>
    {
        IList<LocationLogicItem> All();
        IList<T> All<T>();
        void Save(LocationLogicItem item);
        void Delete(string id);
        bool ValidateLocationName(string locationName);
    }
}
