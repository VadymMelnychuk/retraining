﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.BusinessLogic
{
    public class HotelLogicItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Stars { get; set; }
        public string LocationId { get; set; }
        public string LocationName { get; set; }
    }
}
