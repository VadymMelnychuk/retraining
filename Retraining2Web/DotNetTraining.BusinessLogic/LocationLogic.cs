﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DotNetTraining.DAL.Entities;
using DotNetTraining.DAL.Reprositories;

namespace DotNetTraining.BusinessLogic
{
    public class LocationLogic : BaseLogicImpl<LocationLogicItem, RestLocation>, ILocationLogic
    {
        private readonly LocationsRepo locationsRepo;

        public LocationLogic(LocationsRepo locationsRepo)
        {
            this.locationsRepo = locationsRepo;
        }

        public IList<LocationLogicItem> All()
        {
            List<LocationLogicItem> list = new List<LocationLogicItem>();
            foreach (var item in locationsRepo.All())
                list.Add(Entity2LogicItem(item));
            return list;
        }

        public IList<T> All<T>()
        {
            return locationsRepo.All<T>();
        }

        public void Delete(string id)
        {
            locationsRepo.Delete(id);
        }

        public void Save(LocationLogicItem item)
        {
            locationsRepo.Save(LogicItem2Entity(item));
        }

        public bool ValidateLocationName(string locationName)
        {
            return locationsRepo.ValidateLocationName(locationName);
        }
    }
}
