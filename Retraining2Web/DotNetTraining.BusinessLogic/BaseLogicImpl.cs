﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.BusinessLogic
{
    public class BaseLogicImpl<TLogicItem, TEntity> : IBaseLogicInterface<TLogicItem, TEntity>
    {
        public TLogicItem Entity2LogicItem(TEntity entityItem)
        {
            return Mapper.Map<TEntity, TLogicItem>(entityItem);
        }

        public TEntity LogicItem2Entity(TLogicItem logicItem)
        {
            return Mapper.Map<TLogicItem, TEntity>(logicItem);
        }

    }
}
