﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.BusinessLogic
{
    public interface IBaseLogicInterface<TLogicItem, TEntity>
    {
        TLogicItem Entity2LogicItem(TEntity entityItem);
        TEntity LogicItem2Entity(TLogicItem logicItem);
    }
}
