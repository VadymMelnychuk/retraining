﻿using DotNetTraining.DAL.Entities;
using DotNetTraining.DAL.Reprositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.BusinessLogic
{
    public class HotelLogic : BaseLogicImpl<HotelLogicItem, Hotel>, IHotelLogic
    {
        private readonly HotelsRepo hotels;

        public HotelLogic(HotelsRepo hotels)
        {
            this.hotels = hotels;
        }

        public void Add(HotelLogicItem hotelLogicItem)
        {
            hotels.Add(LogicItem2Entity(hotelLogicItem));
        }

        public List<HotelLogicItem> All()
        {
            return hotels.All().ConvertAll<HotelLogicItem>(h => Entity2LogicItem(h));
        }
        public void Delete(int id)
        {
            hotels.Delete(id);
        }
        public HotelLogicItem HotelById(int id)
        {
            return Entity2LogicItem(hotels.HotelById(id));
        }

        public void Update(int id, HotelLogicItem hotelLogicItem)
        {
            hotels.Update(id, LogicItem2Entity(hotelLogicItem));
        }
    }
}
