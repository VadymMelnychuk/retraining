﻿using System.Collections.Generic;

namespace DotNetTraining.BusinessLogic
{
    public interface IHotelLogic
    {
        List<HotelLogicItem> All();
        void Delete(int id);
        HotelLogicItem HotelById(int id);
        void Add(HotelLogicItem hotelLogicItem);
        void Update(int id, HotelLogicItem hotelLogicItem);
    }
}