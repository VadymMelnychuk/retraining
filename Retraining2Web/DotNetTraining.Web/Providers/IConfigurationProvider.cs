﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.Web.Providers
{
    public interface IConfigurationProvider
    {
        string AgencyLogoFolder { get; }
        string SOAPUser { get; }
        string SOAPPassword { get; }
    }
}
