﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.Web.Providers
{
    class ConfigurationProviderImpl : IConfigurationProvider
    {
        public string SOAPUser => ConfigurationManager.AppSettings["SOAPUser"];

        public string SOAPPassword => ConfigurationManager.AppSettings["SOAPPassword"];

        string IConfigurationProvider.AgencyLogoFolder => ConfigurationManager.AppSettings["AgencyLogoFolder"];

    }
}
