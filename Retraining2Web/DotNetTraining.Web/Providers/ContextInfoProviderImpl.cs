﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DotNetTraining.Web.Providers
{
    public class ContextInfoProviderImpl : IContextInfoProvider
    {
        public DateTime Now => DateTime.Now;
    }
}