﻿using AutoMapper;
using DotNetTraining.ApplicationFacade.HotelServiceReference;
using DotNetTraining.DAL;
using DotNetTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using RemoteHotel = DotNetTraining.ApplicationFacade.HotelServiceReference.Hotel;
using LocalHotel = DotNetTraining.DAL.Entities.Hotel;
using IConfigProvider = DotNetTraining.Web.Providers.IConfigurationProvider;

namespace DotNetTraining.Web.Remote
{
    public class HotelDataSynchronizer
    {
        private const string WSDL_TOKEN_KEY_NAME = "wsdl_access_token";
        private const string LOCATION_LIST = "location_list";
        private const string HOTEL_LIST = "hotel_list";

        private readonly ObjectCache cache;
        private HotelServiceClient client;
        private string token;
        private readonly WebAppDbContext dbContext;
        private readonly IConfigProvider config;

        public HotelDataSynchronizer(WebAppDbContext dbContext, IConfigProvider config)
        {
            this.dbContext = dbContext;
            this.config = config;
            cache = MemoryCache.Default;
        }

        private Location[] GetRemoteLocations()
        {
            if (cache.Contains(LOCATION_LIST))
            {
                return (Location[])cache[LOCATION_LIST];
            }
            else
            {
                Location[] locations = client.GetLocationsList(token, out OperationResult result);
                if (result != OperationResult.Success)
                    throw new MethodAccessException("An error occure during executeion <GetHotelList>");

                cache.Add(LOCATION_LIST, locations, DateTimeOffset.Now.AddHours(3));
                return locations;
            }
        }

        private RemoteHotel[] GetRemoteHotels()
        {
            if (cache.Contains(HOTEL_LIST))
            {
                return (RemoteHotel[])cache[HOTEL_LIST];
            }
            else
            {
                RemoteHotel[] hotels = client.GetHotelList(token, -1, out OperationResult result);
                if (result != OperationResult.Success)
                    throw new MethodAccessException("An error occure during executeion <GetHotelList>");

                cache.Add(HOTEL_LIST, hotels, DateTimeOffset.Now.AddHours(3));
                return hotels;
            }
        }

        private void SyncLocations()
        {
            Location[] remoteLocations = GetRemoteLocations();
            var q =
                from remLocation in remoteLocations
                join loc in dbContext.Locations on remLocation.LocationID equals loc.ExternalId into ps
                from p in ps.DefaultIfEmpty()
                where p is null
                select remLocation;

            var counties =
                from remLocation in remoteLocations
                join country in dbContext.Countries on remLocation.Country equals country.Name into cntr
                from c in cntr.DefaultIfEmpty()
                where c is null
                select remLocation.Country;

            foreach (var cName in counties)
            {
                dbContext.Countries.Add(new RestCountry() { Name = cName });
            }

            foreach (var loc in q)
            {
                dbContext.Locations.Add(Mapper.Map<RestLocation>(loc));
            }            
        }
        private void SyncHotels()
        {
            RemoteHotel[] hotels = GetRemoteHotels();

            var q =
                from remHotel in hotels
                join h in dbContext.Hotels on remHotel.HotelID equals h.ExternalId into pHotels
                from p in pHotels.DefaultIfEmpty()
                where p is null
                select remHotel;

            foreach (var h in q)
            {
                dbContext.Hotels.Add(Mapper.Map<LocalHotel>(h));
            }
        }
        private void SyncPackages()
        {
            Package[] packages = client.GetAvailablePackagesList(token, out OperationResult res);
            //packages[0].
        }
        private void InitRemote()
        {
            this.client = new HotelServiceClient();
            if (cache.Contains(WSDL_TOKEN_KEY_NAME))
            {
                token = cache[WSDL_TOKEN_KEY_NAME].ToString();
            }
            else
            {
                OperationResult res = client.Login(config.SOAPPassword, config.SOAPUser, out token); //"TU!ps$$-wW41hj", "TestUser"
                if (res != OperationResult.Success)
                    throw new MethodAccessException("An error occure during Login: " + res.ToString());

                cache.Add(WSDL_TOKEN_KEY_NAME, token, DateTimeOffset.Now.AddHours(3));
            }
        }
        public void RunSync()
        {
            InitRemote();
            SyncLocations();
            SyncHotels();
            SyncPackages();
        }
    }
}