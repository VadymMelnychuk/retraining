﻿using DotNetTraining.ApplicationFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace DotNetTraining.Web.Helpers
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString LogoImageFor(this HtmlHelper<AgencyFacadeItem> htmlHelper, string imageSrc, string logoId)
        {
            var imageDiv = new TagBuilder("div");
            imageDiv.AddCssClass("col-md-10");

            string inputFieldId = $"input_for_{logoId}";
            var label = new TagBuilder("label");
            label.AddCssClass("floating_label");
            label.MergeAttribute("for", inputFieldId);
            label.MergeAttribute("id", $"label_for_{logoId}");

            var image = new TagBuilder("img");
            image.AddCssClass("img_with_hover_text");
            image.MergeAttribute("id", $"image_for_{logoId}");
            image.MergeAttribute("src", imageSrc);
            image.MergeAttribute("width", "150");
            image.MergeAttribute("height", "150");
            image.MergeAttribute("title", "Change Logo");

            var textbox = InputExtensions.TextBox(htmlHelper, logoId, "", 
                new { type = "file", style = "display:none", id = inputFieldId, accept = "image/png, image/jpeg", oninput = "onInputLogoFileField(event)" }
            );            

            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.Append(image.ToString(TagRenderMode.Normal));
            htmlBuilder.Append(label.ToString(TagRenderMode.Normal));
            htmlBuilder.Append(textbox.ToHtmlString());
            imageDiv.InnerHtml = htmlBuilder.ToString();
            var html = imageDiv.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(html);
            //< div class="col-md-10" id="divAgencyLogo">
            //    <img id = "agencyLogoImg" class="hover_text" src="@Url.Content(@srvLogoSrc)" alt="@Model.Name" title="Change Logo" width="150" height="150" />
            //</div>
            //<input type = "file" name="LogoFile" id="field_LogoFile" value="Upload New Logo" oninput="NewLogoImageName()" style="display:none" accept="image/png, image/jpeg" />
        }
    }
}