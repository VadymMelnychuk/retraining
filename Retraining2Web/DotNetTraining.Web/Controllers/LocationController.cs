﻿using DotNetTraining.ApplicationFacade;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DotNetTraining.Web.Controllers
{
    [Authorize(Roles = "Admins")]
    public class LocationController : Controller
    {
        private readonly ILocationFacade locationFacade;

        public LocationController(ILocationFacade locationFacade)
        {
            this.locationFacade = locationFacade;
        }

        private ActionResult BackToList()
        {
            return RedirectToAction("Index", "Location");
        }

        private ActionResult SaveAndReturnToIndex(LocationFacadeItem model)
        {
            locationFacade.Save(model);
            return BackToList();
        }

        private bool IsLocationNameUnique(string locationName)
        {
            return locationFacade.ValidateLocationName(locationName);
        }
        public ActionResult ValidateLocationName(string locationName)
        {
            return IsLocationNameUnique(locationName) ? Json(true, JsonRequestBehavior.AllowGet) : Json(false, JsonRequestBehavior.AllowGet);
        }
        // GET: Location
        public ActionResult Index()
        {
            return View(locationFacade.All());
        }
        [HttpGet]
        public ActionResult Edit(string id)
        {
            if (id == null)
                return BackToList();

            object model = locationFacade.ItemById(id.ToString());
            return View(model);
        }

        // POST: /Location/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LocationFacadeItem model)
        {
            if ((!ModelState.IsValid) || (!IsLocationNameUnique(model.LocationName)))
                return View();

            return SaveAndReturnToIndex(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View("Edit", new LocationFacadeItem() { Id = Guid.NewGuid().ToString() });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LocationFacadeItem model)
        {
            if ((!ModelState.IsValid) || (!IsLocationNameUnique(model.LocationName)))
                return View("Edit", model);

            return SaveAndReturnToIndex(model);
        }

        [HttpGet]
        public ActionResult Delete()
        {
            locationFacade.Delete(RouteData.Values["id"].ToString());
            return BackToList();
        }
    }
}