﻿using DotNetTraining.ApplicationFacade;
using DotNetTraining.Web.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace DotNetTraining.Web.Controllers
{
    [Authorize(Roles = "Admins,Agents")]
    public class AgencyController : Controller
    {
        private readonly IAgencyFacade agencyFacade;

        public IConfigurationProvider configProvider { get; }

        public AgencyController(IAgencyFacade agencyFacade, IConfigurationProvider configProvider)
        {
            this.agencyFacade = agencyFacade;
            this.configProvider = configProvider;
        }

        private string LogoFolder()
        {
            return Server.MapPath(configProvider.AgencyLogoFolder);
        }
        private string AgencyLogoFile(AgencyFacadeItem agency)
        {
            string folder = Path.Combine(LogoFolder(), agency.Id.ToString());
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            return Path.Combine(folder, agency.LogoImageName);
        }
        [HttpGet]
        public ActionResult Edit()
        {
            var user = HttpContext.User as ClaimsPrincipal;
            int agencyId = int.Parse(user.FindFirst(c => c.Type == ClaimTypes.UserData).Value);

            var agency = agencyFacade.AgencyById(agencyId);
            if (agency == null)
                return new HttpNotFoundResult();

            return View(agency);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AgencyFacadeItem model, HttpPostedFileBase LogoFile)
        {
            if (!ModelState.IsValid)
                return View(model);

            if ((LogoFile != null) && (LogoFile.ContentLength > 0))
            {
                string filePath = AgencyLogoFile(model);
                using (var stream = new FileStream(filePath, FileMode.OpenOrCreate))
                {
                    LogoFile.InputStream.CopyTo(stream);
                }
            }

            agencyFacade.Save(model);
            ViewData["SaveSuccessful"] = "Successful!";
            return View(model);
        }
    }
}