﻿using DotNetTraining.Web.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DotNetTraining.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sync()
        {
            var synchro = DependencyResolver.Current.GetService<HotelDataSynchronizer>();
            synchro.RunSync();            
            return RedirectToAction("Index");
        }
    }
}
