﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetTraining.ApplicationFacade;
using DotNetTraining.BusinessLogic;
using DotNetTraining.DAL.Entities;
using DotNetTraining.DAL.Reprositories;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System.Web.Mvc;

namespace DotNetTraining.Web
{
    public partial class Startup
    {
        void ConfigureAutoMapper()
        {
            Container cnt = (DependencyResolver.Current as SimpleInjectorDependencyResolver).Container;
            Mapper.Initialize(cfg =>
            {
                // Facade <=> Business Logic
                cfg.CreateMap<LocationLogicItem, LocationFacadeItem>();
                cfg.CreateMap<LocationFacadeItem, LocationLogicItem>();
                cfg.CreateMap<AgencyLogicItem, AgencyFacadeItem>();
                cfg.CreateMap<AgencyFacadeItem, AgencyLogicItem>();
                cfg.CreateMap<HotelLogicItem, HotelFacadeItem>();
                cfg.CreateMap<HotelFacadeItem, HotelLogicItem>();

                // Business Logic <=> Data Entity
                cfg.CreateMap<RestLocation, LocationLogicItem>()
                  .ForMember(logicItem => logicItem.Country, map => map.MapFrom(p => p.Country.Name))
                  .ForMember(logicItem => logicItem.CountryId, map => map.MapFrom(p => p.Country.Id))
                  .ForMember(logicItem => logicItem.LocationName, map => map.MapFrom(p => p.DisplayName));
                cfg.CreateMap<LocationLogicItem, RestLocation>()
                  .ForMember(restLocation => restLocation.Country, map => map.ResolveUsing(
                        logicItem => cnt.GetInstance<CountriesRepo>().All().FirstOrDefault(c => c.Id.Equals(logicItem.CountryId)))
                  )
                  .ForMember(restLocation => restLocation.DisplayName, map => map.MapFrom(p => p.LocationName));
                
                cfg.CreateMap<Agency, AgencyLogicItem>();
                cfg.CreateMap<AgencyLogicItem, Agency>();
                cfg.CreateMap<Hotel, HotelLogicItem>()
                  .ForMember(logicItem => logicItem.LocationId, map => map.MapFrom(l => l.Location.Id))
                  .ForMember(logicItem => logicItem.LocationName, map => map.MapFrom(l => l.Location.DisplayName));
                cfg.CreateMap<HotelLogicItem, Hotel>()
                  .ForMember(entity => entity.Location, map => map.ResolveUsing(
                        logicItem => cnt.GetInstance<LocationsRepo>().All().FirstOrDefault(l => l.Id.Equals(logicItem.LocationId))))
                  .ForMember(entity => entity.Rooms, map => map.Ignore())
                  .ForMember(entity => entity.Caterings, map => map.Ignore());

                cfg.CreateMap<Agency, Agency>();
                cfg.CreateMap<Hotel, Hotel>();

                cfg.CreateMap<RestCountry, SelectListItem>()
                    .ForMember(selItem => selItem.Text, map => map.MapFrom(p => p.Name))
                    .ForMember(selItem => selItem.Value, map => map.MapFrom(p => p.Id));
                cfg.CreateMap<RestLocation, KeyValuePair<string, string>>()
                    .ConvertUsing((l, pair) => new KeyValuePair<string, string>(l.Id, l.DisplayName));

                cfg.CreateMap<ApplicationFacade.HotelServiceReference.Hotel, HotelFacadeItem>()
                    .ForMember(facadeItem => facadeItem.Id, map => map.MapFrom(remHotel => remHotel.HotelID))
                    .ForMember(facadeItem => facadeItem.LocationId, map => map.MapFrom(remHotel => remHotel.Location.LocationID.ToString()))
                    .ForMember(facadeItem => facadeItem.LocationName, map => map.MapFrom(remHotel => remHotel.Location.LocationName))
                    .ForMember(facadeItem => facadeItem.Name, map => map.MapFrom(remHotel => remHotel.HotelName))
                    .ForMember(facadeItem => facadeItem.Stars, map => map.MapFrom(remHotel => remHotel.Stars));

                cfg.CreateMap< ApplicationFacade.HotelServiceReference.Location, RestLocation>()
                  .ForMember(restLocation => restLocation.DisplayName, map => map.MapFrom(p => p.LocationName))
                  .ForMember(restLocation => restLocation.Country, map => map.ResolveUsing(
                        remLocation => cnt.GetInstance<CountriesRepo>().All().FirstOrDefault(c => c.Name.Equals(remLocation.Country)))
                  )
                  .ForMember(restLocation => restLocation.City, map => map.MapFrom(p => p.City))
                  .ForMember(restLocation => restLocation.ExternalId, map => map.MapFrom(p => p.LocationID));

                cfg.CreateMap<ApplicationFacade.HotelServiceReference.Hotel, Hotel>()
                  .ForMember(hotel => hotel.Name, map => map.MapFrom(p => p.HotelName))
                  .ForMember(hotel => hotel.Stars, map => map.MapFrom(p => p.Stars))
                  .ForMember(hotel => hotel.ExternalId, map => map.MapFrom(p => p.HotelID))
                  .ForMember(hotel => hotel.Location, map => map.ResolveUsing(
                      remLoc => cnt.GetInstance<LocationsRepo>().All().FirstOrDefault(l => l.ExternalId == remLoc.Location.LocationID)));
            });
        }
    }
}