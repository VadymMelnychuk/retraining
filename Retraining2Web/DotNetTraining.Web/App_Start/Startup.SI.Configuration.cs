﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using DotNetTraining.DAL;
using DotNetTraining.ApplicationFacade;
using DotNetTraining.BusinessLogic;
using DotNetTraining.DAL.Reprositories;
using DotNetTraining.Web.Providers;
using System.Web.Http;
using SimpleInjector.Integration.WebApi;
using DotNetTraining.Web.Remote;

namespace DotNetTraining.Web
{
    public partial class Startup
    {
        void ConfigureSimpleInjector()
        {
            Container container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            // Register your types, for instance:
            container.Register<WebAppDbContext, WebAppDbContext>(Lifestyle.Scoped);

            container.Register<CountriesRepo, CountriesRepo>(Lifestyle.Scoped);
            container.Register<LocationsRepo, LocationsRepo>(Lifestyle.Scoped);
            container.Register<AgencyRepo, AgencyRepo>(Lifestyle.Scoped);
            container.Register<HotelsRepo, HotelsRepo>(Lifestyle.Scoped);

            container.Register<ILocationFacade, LocationFacade>(Lifestyle.Scoped);
            container.Register<IAgencyFacade, AgencyFacade>(Lifestyle.Scoped);
            container.Register<IHotelFacade, HotelFacade>(Lifestyle.Scoped);

            container.Register<ILocationLogic, LocationLogic>(Lifestyle.Scoped);
            container.Register<IAgencyLogic, AgencyLogic>(Lifestyle.Scoped);
            container.Register<IHotelLogic, HotelLogic>(Lifestyle.Scoped);

            // Providers
            container.Register<IConfigurationProvider, ConfigurationProviderImpl>(Lifestyle.Scoped);
            container.Register<IContextInfoProvider, ContextInfoProviderImpl>(Lifestyle.Scoped);

            container.Register<HotelDataSynchronizer, HotelDataSynchronizer>(Lifestyle.Scoped);

            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}