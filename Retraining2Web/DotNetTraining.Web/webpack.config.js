﻿/// <binding BeforeBuild='Run - Development' />
"use strict";
var path = require('path');


module.exports = {
    entry: "./scripts/app/pages/order.js",
    output: {
        filename: "dotnettraining.js",
        path: path.resolve(__dirname, "Scripts/App")
    },
    devServer: {
        contentBase: ".",
        host: "localhost",
        port: 3000
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader', 
                query: {
                    presets: ['babel-preset-react']
                }
            }
        ]
    }
};