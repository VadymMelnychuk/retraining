﻿/// <reference path="../../typing/vue/vue.d.ts" />

export class HotelManagementPageModule {
    public init() {
        console.log("HotelManagementPageModule.init()");

        var vm = new Vue({
            el: '#list-of-hotels',
            data() {
                return {
                    hotelList: [],
                    locationList: [],
                    hotel: { id: 0, name: '', stars: 0, locationId: '', locationName: ''},
                    hotelManagment: 'list'
                };
            },

            methods: {
                getHotels() {
                    let vmThis = this;
                    fetch("/api/Hotel").then(
                        function (response) { // Success.
                            return response.json();
                        },
                        function (response) { // Error.
                            console.log('/api/Hotel: An error occurred.');
                        })
                        .then(function (body) {
                            vmThis.hotelList = body;
                        });
                },

                getLocations() {
                    let vmThis = this;
                    fetch("/api/Location")
                        .then(
                            function (responce) {
                                return responce.json();
                            },
                            function (response) { // Error.
                                console.log('/api/Location: An error occurred.');
                            })
                        .then(function (body) {
                            vmThis.locationList = body;
                        })
                },

                createNewHotel() {
                    console.log('Create Hotel.');
                    let vmThis = this;
                    vmThis.getLocations();
                   // vmThis.hotel = new Hotel;
                    vmThis.hotelManagment = 'edit';
                },

                editHotel(id) {
                    console.log('Edit Hotel.');
                    let vmThis = this;
                    vmThis.getLocations();
                    vmThis.hotelManagment = 'edit';
                    fetch("/api/Hotel/" + id)
                        .then(function (responce) {
                            return responce.json()
                        },
                            function (response) { // Error.
                                console.log('An error occurred.');
                            })
                        .then(function (json) {
                            vmThis.hotel = json
                        })
                },

                cancelEdit() {
                    console.log('Cancel Edit Hotel.');
                    let vmThis = this;
                    vmThis.hotelManagment = 'list';
                },

                saveHotel() {
                    console.log('Save Hotel.');
                    let vmThis = this;
                    vmThis.hotelManagment = 'list';

                    let isNew = vmThis.hotel.id === 0;
                    let httpMeth = isNew ? 'post' : 'put';

                    fetch("api/Hotel/" + vmThis.hotel.id, {
                        method: httpMeth,
                        headers: {
                            'Accept': 'application/json, text/plain, */*',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(vmThis.hotel)
                    });

                    if (!isNew) {
                        for (var index = 0; index < vmThis.hotelList.length; index++) {//(item, index) in vmThis.hotelList) {
                            if (vmThis.hotelList[index].id === vmThis.hotel.id) {
                                vmThis.$set(vmThis.hotelList, index, vmThis.hotel);
                            }
                        }
                    }
                    else {
                        let location = vmThis.locationList.find(function (item) { return item.id === vmThis.hotel.locationId });
                        vmThis.hotel.locationName = location.locationName;
                        vmThis.hotelList.push(vmThis.hotel);
                    }
                },

                editHandler(event) {
                    console.log('Edit Handler.');
                    let vmThis = this;
                    vmThis.editHotel(event.target.hotelId)
                }
            },

            created() {
                this.getHotels();
            },

            components: {
                'edit-button':
                {
                    props: ['hotelId'],
                    template: '<button v-on:click=doEdit>Edit</button>',
                    methods: {
                        doEdit: function () {
                            console.log('doEdit: ', this.hotelId);
                            this.$emit('edit-click', this.hotelId);
                        }
                    }
                },

                'star-template':
                {
                    props: ['stars'],
                    template: '<td><input type="radio" id="five" value="5"/><label for="five" >{{stars}}</label></td>'
                }
            }
        });

        window['vm'] = vm;
    }
}
