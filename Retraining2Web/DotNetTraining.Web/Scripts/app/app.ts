﻿import { HotelManagementPageModule } from './pages/hotel.js';

export class App {
    public static init(): void {
        let pageModuleName = window.location.pathname.toLowerCase().replace('/', '');
        let pageModule = null;
        switch (pageModuleName) {
            case 'hotel': pageModule = new HotelManagementPageModule(); break;
        }

        if (pageModule) {
            pageModule.init();
        }
        else {
            console.error('PageModule "' + pageModuleName + '" not found!');
        }
    }
}
