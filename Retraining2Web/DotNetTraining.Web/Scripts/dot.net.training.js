﻿$(function () {
	var info = $("#label_for_LogoFile");
/*	if (info.length === 0) {
		info = $("<label for=\"field_LogoFile\"/>").addClass("floating_label");
		$("#divAgencyLogo").append(info);
	}*/
	info.hide();
	$(".img_with_hover_text").bind("mouseenter",
		function () {
			var p = GetScreenCordinates(this);
			info.html(this.title);
			info.show();
			info.css("width", $(this).width());
			info.css("height", $(this).height() * 0.3);
			info.css("top", p.y + $(this).height() * 0.7);
		}
	);
	$(".img_with_hover_text").bind("mouseleave", function () {
		info.hide();
	});

	$(".floating_label").bind("mouseenter", function () {
		info.show();
		info.css("opacity", 0.6);
	});

	$(".floating_label").bind("mouseleave", function () {
		info.css("opacity", 0.3);
		info.hide();
	});
});
function GetScreenCordinates(obj) {
	var p = {};
	p.x = obj.offsetLeft;
	p.y = obj.offsetTop;
	return p;
}
function displayUserImage(input, imageField) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onloadend = function (e) {
			imageField.attr('src', e.target.result);
		};
		reader.readAsDataURL(input.files[0]);
	}
}
