﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using DotNetTraining.ApplicationFacade;

namespace DotNetTraining.Web.Api.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Agents, Admins")]
    public class HotelController : ApiController
    {
        private readonly IHotelFacade hotelFacade;

        public HotelController(IHotelFacade hotelFacade)
        {
            this.hotelFacade = hotelFacade;
        }

        // GET: api/Hotel
        public IEnumerable<HotelFacadeItem> GetAll()
        {
            return hotelFacade.All();
        }

        // GET: api/Hotel/5
        public HotelFacadeItem Get(int id)
        {
            return hotelFacade.HotelById(id);
        }

        // POST: api/Hotel
        public void Post([FromBody]HotelFacadeItem value)
        {
            hotelFacade.Add(value);
        }

        // PUT: api/Hotel/5
        public void Put(int id, [FromBody]HotelFacadeItem value)
        {
            hotelFacade.Update(id, value);
        }

        // DELETE: api/Hotel/5
        public void Delete(int id)
        {
            hotelFacade.Delete(id);
        }
    }
}
