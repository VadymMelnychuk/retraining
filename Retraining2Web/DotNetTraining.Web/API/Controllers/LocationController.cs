﻿using DotNetTraining.ApplicationFacade;
using DotNetTraining.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace DotNetTraining.Web.API.Controllers
{
    public class LocationController : ApiController
    {
        private readonly ILocationFacade locationFacade;

        public LocationController(ILocationFacade locationFacade)
        {
            this.locationFacade = locationFacade;
        }

        // GET: api/Location
        public IEnumerable<KeyValuePair<string, string>> GetAll()
        {
            return locationFacade.All<KeyValuePair<string, string>>();
        }
    }
}
