﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.DAL.Entities
{
    public class RestLocation 
    {
        //public RestLocation() => Id = Guid.NewGuid().ToString();
        [Key]
        public string Id { get; set; } 
        public string DisplayName { get; set; }
        public RestCountry Country { get; set; }
        public string City { get; set; }
        public int ExternalId { get; set; }
        public void CopyFrom(RestLocation source)
        {
            DisplayName = source.DisplayName;
            Country = source.Country;
            City = source.City;
        }
    }
}
