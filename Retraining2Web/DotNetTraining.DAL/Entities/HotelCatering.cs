﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.DAL.Entities
{
    public class HotelCatering
    {
        public int Id { get; set; }
        public string CateringType { get; set; }
        public int Price { get; set; }
    }
}
