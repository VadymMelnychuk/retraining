﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.DAL.Entities
{
    public class HotelRoom
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public string RoomType { get; set; }
        public int Count { get; set; }
    }
}
