﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.DAL.Entities
{
    public class PacketOrder
    {
        public int Id { get; set; }
        public Hotel Hotel { get; set; }
        public HotelCatering Catering { get; set; }
        public HotelRoom Room { get; set; }
        public int RoomCount { get; set; }
    }
}
