﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.DAL.Entities
{
    public class Hotel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public RestLocation Location { get; set; }
        public int Stars { get; set; }
        public int ExternalId { get; set; }
        public List<HotelRoom> Rooms { get; set; }
        public List<HotelCatering> Caterings { get; set; }
    }
}
