﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DotNetTraining.DAL.Entities;
using DotNetTraining.DAL.Providers;

namespace DotNetTraining.DAL.Reprositories
{
    public class CountriesRepo: BaseRepo, ICompactEntityListProvider
    {
        public CountriesRepo(WebAppDbContext dbContext): base(dbContext)
        {
        }

        public List<RestCountry> All()
        {
            return dbContext.Countries.ToList();            
        }

        List<T> ICompactEntityListProvider.All<T>()
        {
            return All().ConvertAll<T>(c => Mapper.Map<T>(c));
        }
    }
}
