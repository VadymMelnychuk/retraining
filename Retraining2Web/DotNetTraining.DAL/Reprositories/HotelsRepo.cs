﻿using AutoMapper;
using DotNetTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.DAL.Reprositories
{
    public class HotelsRepo : BaseRepo
    {
        public HotelsRepo(WebAppDbContext dbContext) : base(dbContext)
        {
        }
        public List<Hotel> All()
        {
            return dbContext.Hotels.Include("Location").ToList();
        }
        public void Delete(int id)
        {
            dbContext.Hotels.Remove(HotelById(id));
        }
        public Hotel HotelById(int id)
        {
            return All().Where(h => h.Id == id).FirstOrDefault();
        }

        public void Add(Hotel hotel)
        {
            dbContext.Hotels.Add(hotel);
            dbContext.SaveChanges();
        }

        public void Update(int id, Hotel hotel)
        {
            var foundItem = dbContext.Hotels.First(h => h.Id == id);
            if (foundItem == null)
                throw new KeyNotFoundException();
            Mapper.Map<Hotel, Hotel>(hotel, foundItem);
            dbContext.SaveChanges();
        }
    }
}
