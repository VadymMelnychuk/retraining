﻿using AutoMapper;
using DotNetTraining.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DotNetTraining.DAL.Reprositories
{
    public class AgencyRepo : BaseRepo
    {
        public AgencyRepo(WebAppDbContext dbContext) : base(dbContext)
        {
        }
        public Agency AgencyById(int agencyId)
        {
            return dbContext.Agencies.ToList().FirstOrDefault(a => a.Id == agencyId);
        }
        public void Save(Agency agency)
        {
            Agency foundItem = AgencyById(agency.Id);
            if (foundItem == null)
                dbContext.Agencies.Add(agency);
            else
                Mapper.Map(agency, foundItem);
            dbContext.SaveChangesAsync();
        }
    }
}
