﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DotNetTraining.DAL.Entities;
using DotNetTraining.DAL.Providers;

namespace DotNetTraining.DAL.Reprositories
{
    public class LocationsRepo : BaseRepo, ICompactEntityListProvider
    {
        public LocationsRepo(WebAppDbContext dbContext) : base(dbContext)
        {
        }
        public List<RestLocation> All()
        {
            return dbContext.Locations.Include("Country").ToList();
        }
        public void Save(RestLocation item)
        {
            RestLocation foundItem = All().Find(x => x.Id == item.Id);
            if (foundItem == null)
                dbContext.Locations.Add(item);
            else
                foundItem.CopyFrom(item);
            dbContext.SaveChangesAsync();
        }
        public void Delete(string id)
        {
            var item = dbContext.Locations.First(x => x.Id == id);
            if (item != null)
            {
                dbContext.Locations.Remove(item);
                dbContext.SaveChangesAsync();
            }              
        }
        public bool ValidateLocationName(string locationName)
        {
            return dbContext.Locations.Where(l => l.DisplayName.Equals(locationName)).Count() <= 0;
        }

        public List<T> All<T>()
        {
            return All().ConvertAll(l => Mapper.Map<T>(l));
        }
    }
}
