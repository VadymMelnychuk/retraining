﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.DAL.Reprositories
{
    public abstract class BaseRepo
    {
        protected readonly WebAppDbContext dbContext;

        public BaseRepo(WebAppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
    }
}
