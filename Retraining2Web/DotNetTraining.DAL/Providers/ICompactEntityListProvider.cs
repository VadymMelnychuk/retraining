﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetTraining.DAL.Providers
{
    public interface ICompactEntityListProvider
    {
        List<T> All<T>();
    }
}
