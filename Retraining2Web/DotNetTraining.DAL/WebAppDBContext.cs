﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using DotNetTraining.DAL.Entities;
using DotNetTraining.DAL.ModelConfigurations;

namespace DotNetTraining.DAL
{
    public class ApplicationUser : IdentityUser
    {
        public string Hometown { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            if (!userIdentity.HasClaim(c => c.Type == ClaimTypes.UserData))
            {
                var agencyId = manager.Users.Include("Agency").Where(u => u.Id == this.Id).Select(x => (int?)x.Agency.Id).FirstOrDefault() ?? 0;
                var claim = new Claim(ClaimTypes.UserData, agencyId.ToString());
                userIdentity.AddClaim(claim);
            }

            return userIdentity;
        }
        public Agency Agency { get; set; }
    }

    public class WebAppDbContext : IdentityDbContext<ApplicationUser>
    {
        public WebAppDbContext()
            : base("Name=WebAppConnectionString", throwIfV1Schema: false)
        {
            Database.SetInitializer<WebAppDbContext>(new WebAppDBInitializer());
        }

        public static WebAppDbContext Create()
        {
            return new WebAppDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new AppUserConfiguration());
        }

        public IPasswordHasher PasswordHasher { get; set; }

        public DbSet<RestLocation> Locations { get; set; }
        public DbSet<RestCountry> Countries { get; set; }
        public DbSet<Agency> Agencies { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<PacketOrder> PacketOrders { get; set; }
    }
}
