﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using DotNetTraining.DAL.Migrations;
using System.Web;
using DotNetTraining.DAL.Entities;

namespace DotNetTraining.DAL
{
    class WebAppDBInitializer : MigrateDatabaseToLatestVersion<WebAppDbContext, Configuration>
    {
        void NewPair(WebAppDbContext context, string roleName, string userName, string agencyName = null)
        {
            IdentityRole r = new IdentityRole() { Name = roleName };
            context.Roles.Add(r);
            string pwdHash = context.PasswordHasher?.HashPassword(userName);

            Agency agency = null;
            if (!string.IsNullOrEmpty(agencyName))
            {
                agency = new Agency() { Name = agencyName, Description = $"Description of {agencyName}" };
            }

            ApplicationUser u = new ApplicationUser() {
                UserName = $"{userName}@{userName}.com",
                Email = $"{userName}@{userName}.com",  // default user email is username@username.com (for example admin@admin.com)
                PasswordHash = pwdHash,
                SecurityStamp = Guid.NewGuid().ToString(),
                Agency = agency,
                EmailConfirmed = true };
            context.Users.Add(u);
            IdentityUserRole u2r = new IdentityUserRole() { UserId = u.Id, RoleId = r.Id };
            u.Roles.Add(u2r);
            context.SaveChanges();
        }

        public override void InitializeDatabase(WebAppDbContext context)
        {
            base.InitializeDatabase(context);

            if ((context.Users.Count() == 0) || (context.Roles.Count() == 0))
            {
                NewPair(context, "Admins", "superadmin", "Admin Agency");
                NewPair(context, "Agents", "agent", "New Agency of Agent");
                NewPair(context, "Tourists", "tourist");
            }

            if (context.Countries.Count() == 0)
            {
                context.Countries.AddRange(new []
                {
                    new RestCountry() { Id = Guid.NewGuid().ToString(), Name = "Ukraine" },
                    new RestCountry() { Id = Guid.NewGuid().ToString(), Name = "Greece" },
                    new RestCountry() { Id = Guid.NewGuid().ToString(), Name = "Italy" },
                    new RestCountry() { Id = Guid.NewGuid().ToString(), Name = "Portugal" },
                    new RestCountry() { Id = Guid.NewGuid().ToString(), Name = "Turkey" },
                    new RestCountry() { Id = Guid.NewGuid().ToString(), Name = "Egypt" }
                });
            }

        }
    }
}
