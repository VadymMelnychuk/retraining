﻿using System.Collections.Generic;

namespace FirstClassLib
{
    interface IOrderList: IDictionary<string, IOrder>
    {
        void AddProductToOrder(string orderId, IProduct product);
    }
}