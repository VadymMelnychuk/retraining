﻿using FirstClassLib.InMemoryDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib
{
    class OrderList : Dictionary<string, Order>
    {
        public void AddProductToOrder(string orderId, Product product)
        {
            Order order;
            if (TryGetValue(orderId, out order))
            {
                order.Details.Add(new OrderDetail() { Product = product, Count = 1});
            }
            else
            {
                throw new Exception($"order '{orderId}' not found.");
            }
        }

    }
}
