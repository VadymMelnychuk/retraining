﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.InMemoryDB
{
    class InMemoryDB
    {
        public ProductList Products { get; set; }
        public IOrderList Orders { get; set; }
    }
}
