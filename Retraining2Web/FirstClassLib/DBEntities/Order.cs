﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib
{
    public class Order 
    {
        public Order()
        {
            this.OrderId = Guid.NewGuid().ToString();
        }

        [MapPropertyToDBAttribute]
        [KeyAttribute]
        public string OrderId { get; set; }
        [MapPropertyToDBAttribute]
        public string Comment { get; set; }
        [MapPropertyToDBAttribute]
        public string DeliveryInfo { get; set; }
        [MapPropertyAsManyAttribute]
        public List<OrderDetail> Details { get; set; } = new List<OrderDetail>();
        [MapPropertyByIdAttribute]
        public Client Client { get; set; }
        public float TotalPrice
        {
            get
            {
                float total = 0;
                foreach (OrderDetail detail in Details)
                    {
                        total += detail.Product.Price;
                    }
                return total;
            }
        }

        public override string ToString()
        {
            return $"ID = {OrderId}; Delivery Info = {DeliveryInfo}; Total Price = {TotalPrice}";
        }
    }
}
