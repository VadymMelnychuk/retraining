﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib
{
    public interface IProduct
    {
        string Name { get; set; }
        string Description { get; set; }
        float Price { get; set; }
    }
}
