﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib
{
    public class Client 
    {
        public Client()
        {
            ClientID = new Random().Next();
        }

        [MapPropertyToDBAttribute]
        [Key]
        public int ClientID { get; set; }
        [MapPropertyToDBAttribute]
        public string Name { get; set; }
        [MapPropertyToDBAttribute]
        public string Phone { get; set; }
        [MapPropertyToDBAttribute]
        public string EMail { get; set; }
    }
}
