﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib
{
    public interface IOrder
    {
        string OrderId { get; set; }
        string Comment { get; set; }
        string DeliveryInfo { get; set; }
        List<IOrderDetail> Details { get; }

        float TotalPrice { get; }
    }
}
