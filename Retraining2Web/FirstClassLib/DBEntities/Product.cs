﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib
{
    public class Product 
    {
        public Product()
        {
            this.ProductId = Guid.NewGuid().ToString();
        }

        [Key]
        [MapPropertyToDB("Id")]
        public string ProductId { get; set; }
        [MapPropertyToDB]
        public string Name { get; set; }
        [MapPropertyToDB("Info")]
        public string Description { get; set; }
        [MapPropertyToDB]
        public float Price { get; set; }
        public override string ToString()
        {
            return $"ID={ProductId}; Name={Name}; Price={Price}";
        }
    }
}
