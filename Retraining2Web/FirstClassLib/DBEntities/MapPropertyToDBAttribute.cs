﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib
{
    class MapPropertyToDBAttribute : Attribute
    {
        public MapPropertyToDBAttribute(string fieldName)
        {
            FieldName = fieldName;
        }

        public MapPropertyToDBAttribute()
        {
            FieldName = "";
        }

        public string FieldName { get; set; }
    }
}
