﻿namespace FirstClassLib
{
    public interface IOrderDetail
    {
        int Id { get; set; }
        int Count { get; set; }
        IProduct Product { get; set; }
    }
}