﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib
{
    public class OrderDetail
    {
        public OrderDetail()
        {
            Id = new Random().Next();
        }

        [Key]
        [MapPropertyToDB]
        public int Id { get; set; }
        [MapPropertyById]
        public Product Product { get; set; }
        [MapPropertyToDB]
        public int Count { get; set; }

        public override string ToString()
        {
            return $"Product = {Product.Name}; Count = {Count}";
        }
    }
}
