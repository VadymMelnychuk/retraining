﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FirstClassLib
{
    class PropertyMapInfo<T>
    {
        public PropertyMapInfo(PropertyInfo prop, string fieldName, Func<T, object> getVal)
        {
            Prop = prop;
            FieldName = fieldName;
            GetVal = getVal;
        }
        public PropertyInfo Prop { get; set; }
        public string FieldName { get; set; }
        public Func<T, Object> GetVal { get; set; }
    }
    class EntityToDataTableConverter<T> where T : class
    {
        private List<PropertyMapInfo<T>> mapProps;
        private List<T> entities;

        public EntityToDataTableConverter()
        {
            PropertyInfo[] allProperties = typeof(T).GetProperties();
            mapProps = new List<PropertyMapInfo<T>>();
            entities = new List<T>();

            // simple fields
            ParameterExpression param = Expression.Parameter(typeof(T));
            var qry = from p in allProperties
                      where (p.GetCustomAttributes<MapPropertyToDBAttribute>().Count() > 0)
                      let a = p.GetCustomAttribute<MapPropertyToDBAttribute>()
                      let e = Expression.TypeAs(Expression.Property(param, p), typeof(Object))
                      let propertyMapInfo = new PropertyMapInfo<T>
                                              (p, a.FieldName != "" ? a.FieldName : p.Name,
                                              Expression.Lambda<Func<T, Object>>(e, param).Compile())
                      select propertyMapInfo
                      ;
            foreach (var mapPropInfo in qry)
            {
                mapProps.Add(mapPropInfo);
            }

            // references
            qry = from p in allProperties
                  where (p.GetCustomAttributes<MapPropertyByIdAttribute>().Count() > 0)
                  let refProp = (from refTypeProp in p.PropertyType.GetProperties()
                                 where (refTypeProp.GetCustomAttributes<KeyAttribute>().Count() > 0)
                                 select refTypeProp).First()
                  let propertyMapInfo = (new PropertyMapInfo<T>
                                      (refProp, $"{p.Name}_{refProp.Name}", obj => { var o = p.GetValue(obj); return o == null ? null : refProp.GetValue(o); })
                                    )
                  select propertyMapInfo;
            foreach (var mapPropInfo in qry)
            {
                mapProps.Add(mapPropInfo);
            }

            // List<>
            //var qryArrayProps = from p in allProperties
            //                    where (p.PropertyType.IsArray && p.PropertyType.IsGenericType && p.GetCustomAttributes<MapPropertyAsManyAttribute>().Count() > 0 && p.PropertyType.GetGenericArguments().Count() > 0)
            //                    let genType = p.PropertyType.GetGenericArguments()[0]
            //                    let pair = (new KeyValuePair<PropertyInfo, Type>(p, genType))
            //                    select pair;
            //foreach (var pair in qryArrayProps)
            //{
            //    Activator.
            //    Activator.CreateInstance(pair.Value);

            //}
        }

        private DataTable GetDataTable()
        {
            var dt = new DataTable(typeof(T).Name + "s", "dbo");

            foreach (var mapPropInfo in mapProps)
                dt.Columns.Add(mapPropInfo.FieldName, mapPropInfo.Prop.PropertyType);

            return dt;
        }

        public void AddEntity(T obj)
        {
            entities.Add(obj);
        }

        public void ConvertEntities2DataTables()
        {
            DataTable dataTable = GetDataTable();
            foreach (T obj in entities)
            {
                var row = dataTable.NewRow();
                foreach (var info in mapProps)
                {
                    row[info.FieldName] = info.GetVal(obj);
                }
                dataTable.Rows.Add(row);
            }
            Tables.Add(dataTable);
        }

        public List<DataTable> Tables { get; set; } = new List<DataTable>();
    }
}
