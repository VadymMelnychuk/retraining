﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.EntityFramework
{
    public class EntityFrameworkTests
    {
        private EFProductsAndOrders db;
        private ExtendedRandom rnd;
        private int CLIENT_COUNT = 10;
        private int PRODUCT_COUNT = 100;
        private int ORDER_COUNT = 1000;
        private int DETAILS_COUNT = 10;

        public EntityFrameworkTests()
        {
            db = new EFProductsAndOrders();
            rnd = new ExtendedRandom();
        }

        private Product DBGenerateNewProduct()
        {
            Product product = new Product()
            {
                Name = rnd.NextString(5),
                Price = (float)rnd.NextDouble() * 100
            };
            return product;
        }

        public void Test_AddProduct()
        {
            Product product = DBGenerateNewProduct();
            db.Products.Add(product);
            db.SaveChanges();
            Console.WriteLine($"Added new product to the DB: {product.ToString()}");

            Order order = new Order()
            {
                Comment = "new",
                Details = { new OrderDetail() { Count = 1, Product = product } }
            };
            db.Orders.Add(order);
            db.SaveChanges();
            Console.WriteLine($"Added new order with one product to the DB: {order.ToString()}");

            Console.ReadKey();
            Console.WriteLine();
        }

        public void Test_DispalyProducts()
        {
            Console.WriteLine("Reading products from the DB:");
            var qryProducts = from product in db.Products
                              select product;
            foreach (var product in qryProducts)
                Console.WriteLine(product.ToString());

            Console.WriteLine("Reading Orders from the DB:");
            var qryOrders = from order in db.Orders
                            select order;
            foreach (var order in qryOrders)
            {
                Console.WriteLine(order.ToString());
                foreach (var d in order.Details)
                    Console.WriteLine($"    *{d.ToString()}");
            }
            Console.ReadKey();
            Console.WriteLine();
        }

        private void PerfomanceTestRunnerEF<TestType>(int count, Action testBody) where TestType : class
        {
            Stopwatch tmr = new Stopwatch();

            // Insert 10 Clients
            tmr.Start();
            for (int i = 0; i < count; i++)
                testBody();
            db.SaveChanges();
            tmr.Stop();
            Console.WriteLine($"Inserted {count} objects of type {typeof(TestType).Name}; Spent Time = {tmr.Elapsed}");
        }

        private void PerfomanceTestRunnerSqlBulk<TestType>(SqlBulkCopy sbc, int count, Func<TestType> entityGenerator) where TestType : class
        {
            Stopwatch tmr = new Stopwatch();

            tmr.Start();

            var converter = new EntityToDataTableConverter<TestType>();
            for (int i = 0; i < count; i++)
                converter.AddEntity(entityGenerator());

            converter.ConvertEntities2DataTables();

            foreach (DataTable dt in converter.Tables)
            {
                sbc.DestinationTableName = dt.TableName;
                sbc.WriteToServer(dt);
            }

            tmr.Stop();
            Console.WriteLine($"Inserted {count} objects of type {typeof(TestType).Name}; Spent Time = {tmr.Elapsed}");
        }

        private void PerfomanceTest_UsingEF(bool trackChanges)
        {
            Console.WriteLine($"Speed test. Track changes={trackChanges}");
            // Insert CLIENT_COUNT Clients
            db.Configuration.AutoDetectChangesEnabled = trackChanges;
            PerfomanceTestRunnerEF<Client>(CLIENT_COUNT, () => db.Clients.Add(new Client() { Name = rnd.NextString(rnd.Next(4, 10)) }));

            //Insert PRODUCT_COUNT products
            db.Configuration.AutoDetectChangesEnabled = trackChanges;
            PerfomanceTestRunnerEF<Product>(PRODUCT_COUNT, () => db.Products.Add(DBGenerateNewProduct()));

            //Insert ORDER_COUNT orders with DETAILS_COUNT details
            db.Configuration.AutoDetectChangesEnabled = trackChanges;
            PerfomanceTestRunnerEF<Order>(ORDER_COUNT, () =>
                {
                    Order order = new Order()
                    {
                        Client = db.Clients.AsEnumerable().ElementAt(rnd.Next(db.Clients.Count())),
                        Comment = rnd.NextString(10)
                    };
                    for (int i = 0; i < DETAILS_COUNT; i++)
                        order.Details.Add(new OrderDetail() { Count = rnd.Next(10), Product = db.Products.AsEnumerable().ElementAt(rnd.Next(db.Products.Count())) });
                    db.Orders.Add(order);
                });
            Console.WriteLine($"Each order has {DETAILS_COUNT} order detail(s).");
            Console.WriteLine();
            Console.ReadKey();
        }


        private void PerfomanceTest_UsingBulk()
        {
            Console.WriteLine($"Speed test. SqlBulkCopy");

            List<Client> clients = new List<Client>();
            List<Product> products = new List<Product>();
            
            SqlBulkCopy sbc = new SqlBulkCopy(db.Database.Connection.ConnectionString);
            // Insert CLIENT_COUNT Clients
            PerfomanceTestRunnerSqlBulk<Client>(sbc, CLIENT_COUNT, () => {
                Client c = new Client() { Name = rnd.NextString(rnd.Next(4, 10)) };
                clients.Add(c);
                return c;
            });

            // Insert PRODUCT_COUNT Products
            PerfomanceTestRunnerSqlBulk<Product>(sbc, PRODUCT_COUNT, () => {
                Product p = DBGenerateNewProduct();
                products.Add(p);
                return p;
            });

            // Insert ORDER_COUNT orders with DETAILS_COUNT details
            PerfomanceTestRunnerSqlBulk<Order>(sbc, ORDER_COUNT, () => {
                Client c = clients.ElementAt(rnd.Next(clients.Count()));
                Product p = products.ElementAt(rnd.Next(products.Count()));
                Order order = new Order()
                {
                    Client = c,
                    Comment = rnd.NextString(10)
                };
                for (int i = 0; i < DETAILS_COUNT; i++)
                    order.Details.Add(new OrderDetail() { Count = rnd.Next(10), Product = p });
                return order;
            });

            Console.WriteLine();
            Console.ReadKey();
        }

        public void Test_InsertPerfomance()
        {
            PerfomanceTest_UsingEF(false);
            PerfomanceTest_UsingEF(true);
            //PerfomanceTest_UsingBulk();
        }

    }
}
