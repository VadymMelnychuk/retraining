﻿using log4net;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.EntityFramework
{
    public class TestNPlus1Problem
    {
        private Container container;
        public TestNPlus1Problem()
        {
            container = new Container();
            container.Register<EFProductsAndOrders>(Lifestyle.Singleton);
            container.Register<IAggregatedCalculation, AggregatedCalculation>();
            //container.Register<IAggregatedCalculation, AggregatedCalculationWithInclude>();
            container.Verify();
        }

        private Order RandomOrder
        {
            get
            {
                EFProductsAndOrders cntx = container.GetInstance<EFProductsAndOrders>();
                return cntx.Orders.AsEnumerable().ElementAt((new Random()).Next(cntx.Orders.Count()));
            }
        }

        private Client RandomClient {
            get
            {
                EFProductsAndOrders cntx = container.GetInstance<EFProductsAndOrders>();
                return cntx.Clients.AsEnumerable().ElementAt((new Random()).Next(cntx.Clients.Count()));
            }
        }
        public void CalculateTests()
        {
            IAggregatedCalculation calc = container.GetInstance<IAggregatedCalculation>();
            calc.OrderCost(RandomOrder.OrderId);

            calc.Print10OrderCostByClient(RandomClient);

            calc.PrintAllClientsWithTotalCosts();
        }
    }
}
