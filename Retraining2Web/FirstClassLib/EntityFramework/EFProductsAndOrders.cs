﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.EntityFramework
{
    public class EFProductsAndOrders : DbContext
    {
        public EFProductsAndOrders() : base("name=EFProductsAndOrdersConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EFProductsAndOrders, Migrations.Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProductEntityConfiguration());
            modelBuilder.Configurations.Add(new OrderEntityConfiguration());
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Client> Clients { get; set; }

    }
}
