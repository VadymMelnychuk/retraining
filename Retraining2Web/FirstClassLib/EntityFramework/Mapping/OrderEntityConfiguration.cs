﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.EntityFramework
{
    class OrderEntityConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderEntityConfiguration()
        {
            Ignore(p => p.TotalPrice);
            HasMany(p => p.Details);            
        }
    }
}
