﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.EntityFramework
{
    class ProductEntityConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductEntityConfiguration()
        {
            Property(p => p.ProductId).HasColumnName("Id");
            Property(p => p.Description).HasColumnName("Info");
        }
    }
}
