﻿namespace FirstClassLib.EntityFramework
{
    interface IAggregatedCalculation
    {
        float OrderCost(string orderId);
        void Print10OrderCostByClient(Client client);
        void PrintAllClientsWithTotalCosts();
    }
}