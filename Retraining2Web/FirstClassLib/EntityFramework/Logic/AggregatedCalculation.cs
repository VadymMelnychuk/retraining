﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.EntityFramework
{
    class AggregatedCalculation : IAggregatedCalculation
    {
        private EFProductsAndOrders context;

        public AggregatedCalculation(EFProductsAndOrders context)
        {
            this.context = context;
        }

        public float OrderCost(string orderId)
        {
            Stopwatch tmr = new Stopwatch();
            tmr.Start();

            Order order = (from o in context.Orders
                           where o.OrderId.Equals(orderId)
                           select o).First();
            float cost = order.Details.Sum(detail => detail.Product.Price * detail.Count);

            tmr.Stop();
            Console.WriteLine($"Order cost calculation time: {tmr.Elapsed}");

            return cost;            
        }

        public void Print10OrderCostByClient(Client client)
        {
            Stopwatch tmr = new Stopwatch();
            tmr.Start();

            var qry = from order in context.Orders
                      where order.Client.ClientID.Equals(client.ClientID)
                      let sum = order.Details.Sum(detail => detail.Count * detail.Product.Price)
                      orderby sum descending
                      select new { Order = order, TotalPrice = sum};

            Console.WriteLine($"Client {client.Name}");
            foreach (var item in qry.Take(10))
            {
                Console.WriteLine(item.TotalPrice.ToString("N3"));
            }
            tmr.Stop();
            Console.WriteLine($"10 orders by client with Total Price. Calculation Time: {tmr.Elapsed}");
        }

        public void PrintAllClientsWithTotalCosts()
        {
            Stopwatch tmr = new Stopwatch();
            tmr.Start();
            var qry = from order in context.Orders
                      group order by order.Client into g
                      let sum = g.Sum(order => order.Details.Sum(detail => detail.Count * detail.Product.Price))
                      select new { Client = g.Key, TotalPrice = sum };


            foreach (var item in qry)
            {
                Console.WriteLine($"Client: {item.Client.Name}; Total Spend: {item.TotalPrice.ToString("N3")}");
            }
            tmr.Stop();
            Console.WriteLine($"Total spend by Client. Calculation Time: {tmr.Elapsed}");
        }
    }
}
