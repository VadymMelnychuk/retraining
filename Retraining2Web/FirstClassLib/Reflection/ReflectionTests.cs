﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace FirstClassLib.Reflection
{
    public class ReflectionTests
    {
        void PrintHashSet<T>(HashSet<T> hashSet)
        {
            Console.WriteLine();
            Console.WriteLine("Items from hashSet:");
            foreach (T obj in hashSet)
                Console.WriteLine(obj.ToString());

        }
        public void Test1()
        {
            Console.WriteLine();
            Console.WriteLine("Test 1: Equality Comparer");
            List<DummyReflectionClass> testList = new List<DummyReflectionClass>()
            {
                new DummyReflectionClass() { Id = 1, Name = "FirstObject", Description = "This is a first object" },
                new DummyReflectionClass() { Id = 2, Name = "FirstObject", Description = "This is a second object" },
                new DummyReflectionClass() { Id = 1, Name = "SecondObject", Description = "This is a object number 3" },
                new DummyReflectionClass() { Id = 1, Name = "SecondObject", Description = "This is an duplicate object" }
            };

            HashSet<DummyReflectionClass> hashSet = new HashSet<DummyReflectionClass>(new EqualityComparerUsingAttribute<DummyReflectionClass>());
            Console.WriteLine("Trying to add Dummy objects: ");
            foreach (DummyReflectionClass obj in testList)
            {
                Console.WriteLine(obj.ToString());
                hashSet.Add(obj);
            }

            PrintHashSet<DummyReflectionClass>(hashSet);
            Console.ReadKey();
        }

        public void Test2()
        {
            Console.WriteLine();
            Console.WriteLine("Test 2: Class Filler with Random Value");

            RandomClassFactory<DummyReflectionClass> fact = new RandomClassFactory<DummyReflectionClass>();
            HashSet<DummyReflectionClass> hashSet = new HashSet<DummyReflectionClass>(new EqualityComparerUsingAttribute<DummyReflectionClass>());

            int count = 1000000;
            Stopwatch tmr = new Stopwatch();
            tmr.Start();
            for (int i = 0; i < count; i++)
            {
                hashSet.Add(fact.Generate());
            }
            tmr.Stop();
            Console.WriteLine($"Generated: {count}; Added: {hashSet.Count}; Working time: {tmr.Elapsed}");
            Console.ReadKey();
        }
    }
}
