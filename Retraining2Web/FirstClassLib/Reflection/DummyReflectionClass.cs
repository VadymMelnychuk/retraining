﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.Reflection
{
    class DummyReflectionClass
    {
        [UseForEqualityCheck]
        public int Id { get; set; }

        [UseForEqualityCheck]
        public string Name { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public int ParentId { get; set; }
        public int Count { get; set; }
        public bool Processed { get; set; }
        public override string ToString()
        {
            return $"ID = {Id}; Name = {Name}; Desc = {Description}; UserID = {UserId}; ParentID = {ParentId}; Count = {Count}; Processed = {Processed}";
        }
    }
}
