﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.Reflection
{
    class EqualityComparerUsingAttribute<T> : IEqualityComparer<T>
    {
        List<Func<T, Object>> delegates;
        public EqualityComparerUsingAttribute()
        {
            delegates = new List<Func<T, Object>>();
            ParameterExpression param = Expression.Parameter(typeof(T));
            foreach (PropertyInfo prop in typeof(T).GetProperties())
            {
                if (prop.GetCustomAttributes<UseForEqualityCheckAttribute>().Count() > 0)
                {
                    Expression exprProp = Expression.TypeAs(Expression.Property(param, prop), typeof(Object));
                    delegates.Add(Expression.Lambda<Func<T, Object>>(exprProp, param).Compile());
                    //delegates.Add((obj) => (prop.GetValue(obj)));
                }
            }
        }

        public bool Equals(T x, T y)
        {
            bool isEqual = false;
            foreach (Func<T, Object> func in delegates)
            {
                isEqual = func(x).Equals(func(y));
                if (!isEqual)
                    break;
            }
            return isEqual;
        }

        public int GetHashCode(T obj)
        {
            int hash = 23;
            foreach (Func<T, Object> func in delegates)
            {
                hash = hash * 31 + func(obj).GetHashCode();
            }
            return hash;
        }
    }
}
