﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FirstClassLib.Reflection
{
    class RandomClassFactory<T> where T : new()
    {
        private ExtendedRandom rnd;
        private List<PropertyInfo> props;

        public RandomClassFactory()
        {
            this.rnd = new ExtendedRandom();
            props = new List<PropertyInfo>();
            foreach (PropertyInfo prop in typeof(T).GetProperties())
                if (!prop.Name.EndsWith("id", StringComparison.CurrentCultureIgnoreCase))
                    props.Add(prop);
        }

        public T Generate()
        {
            T result = new T();
            foreach (PropertyInfo prop in props)
            {
                if (prop.PropertyType == typeof(int))
                {
                    prop.SetValue(result, rnd.Next());
                }
                else
                    if (prop.PropertyType == typeof(float))
                {
                    prop.SetValue(result, rnd.NextDouble());
                }
                else
                    if (prop.PropertyType == typeof(string))
                {
                    prop.SetValue(result, rnd.NextString(rnd.Next(3, 15)));
                }
                else
                    if (prop.PropertyType == typeof(bool))
                {
                    prop.SetValue(result, (rnd.Next(0, 100) < 50 ? false : true));
                }
            }
             return result;
        }
}
}
